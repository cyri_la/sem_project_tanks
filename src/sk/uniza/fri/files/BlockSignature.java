package sk.uniza.fri.files;

/**
 * 28/03/2022 - 08:19
 * Enumeracia pre ukladenie do do bin suboru
 * typ bloku zapise ako string
 */
public enum BlockSignature {

    MINE("m"), MINE_DISABLED("d"), GROUND("g"), WALL("w"), BUNKER("b"), WALL_DISABLED("a");

    private final String signature;

    /**
     * Konstruktor pre enum BlockSignature
     * @param signature oznacenie, ktore bude zapisane v bin file
     */
    BlockSignature(String signature) {
        this.signature = signature;
    }

    /**
     * Getter pre signature
     * @return vracia signature
     */
    public String getSignature() {
        return this.signature;
    }
}
