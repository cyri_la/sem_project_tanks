package sk.uniza.fri.files;

/**
 * 28/03/2022 - 08:19
 * Vynimka pre nekompatibilne verzie hry
 */
public class IncompatibleGameVersionException extends Throwable {
    /**
     * Konstruktor pre vynimku IncompatibleGameVersionException
     * obsahuje super a message
     */
    public IncompatibleGameVersionException() {
        super("Game version is incompatible, cannot load the save");
    }

}
