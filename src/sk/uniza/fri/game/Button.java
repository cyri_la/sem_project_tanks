package sk.uniza.fri.game;

/**
 * 28/03/2022 - 08:19
 * Trieda Button predstavuje tlacidlo na stranke - user interface
 * optimalizovany reuse kodu z predchadzajusej hry: Attack on Pearl Harbor
 * optimalizacia: pouzitie dedicnosti na prvok, ktory "je na stranke", spolocne znaky medzi buttonom a titulkom
 */
public class Button extends OnPage {
    
    private final ButtonMeaning buttonMeaning;
    /**
     * Konstruktor triedy Button
     * tlacidlo, pomocou obrazka zobrazi na x,y
     * tlacidlo ma svoj vyznam
     */
    public Button(int x, int y, int sizeX, int sizeY, String file, ButtonMeaning buttonMeaning) {
        // initialise instance variables
        super(x, y, sizeX, sizeY, file);
        this.buttonMeaning = buttonMeaning;

    }
    /**
     * Getter pre vyznam tlacidla
     */
    public ButtonMeaning getButtonMeaning() {

        return this.buttonMeaning;
    }
}
