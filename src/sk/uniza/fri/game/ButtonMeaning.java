package sk.uniza.fri.game;

/**
 * 28/03/2022 - 08:19
 *
 *Trieda ButtonMeaning pre spravu vyznamu tlacidla
 * optimalizovany reuse kodu pre Blitzkreig z hry: Attack on Preal Harbor
 *
 **/
public enum ButtonMeaning {
    EASY(10, 10, 70),
    MEDIUM (11, 11, 60),
    HARD(12, 12, 50),
    RESTART(),
    LOADGAME(),
    SAVEGAME(),
    EXITGAME();

    private final int numberOfWall;
    private final int numberOfMines;
    private final int numberOfBullets;
    /**
     * Konstruktor pre triedu ButtonMeaning
     */
    ButtonMeaning(int numberOfWall, int numberOfMines, int numberOfBullets) {
        this.numberOfWall = numberOfWall;
        this.numberOfMines = numberOfMines;
        this.numberOfBullets = numberOfBullets;
    }

    /**
     * Druhy kostruktor pre EXIT, RESTART, kde nepotrebujeme cisla
     */
    ButtonMeaning() {
        this.numberOfWall = 0;
        this.numberOfMines = 0;
        this.numberOfBullets = 0;
    }
    /**
     * Getter pre atribut numberOfWall
     * pocet blokov steny
     */
    int getNumberOfWall() {
        return this.numberOfWall;
    }
    /**
     * Getter pre atribut numberOfMines
     * pocet blokov miny
     */
    int getNumberOfMines() {
        return this.numberOfMines;
    }
    /**
     * Getter pre atribut numberOfBullets
     * pocet nabojov pre tank
     */
    int getNumberOfBullets() {
        return this.numberOfBullets;
    }
}
