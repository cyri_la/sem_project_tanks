package sk.uniza.fri.game;

import java.util.ArrayList;

/**
 * 28/03/2022 - 08:19
 *
 * Trieda Display pre zobrazenie ciselnych udajov
 * optimalizovany reuse kodu pre Blitzkreig z Attack on Pearl Harbor
 *
 */
public class Display {

    private int x;
    private int y;
    private int size;
    private ArrayList<Number> numbers;

    /**
     * Konstruktor pre objekty Display
     * @param x HL bod
     * @param y HL bod
     * @param sizeOfNum velkost cisel
     */
    public Display(int x, int y, int sizeOfNum) {
        // initialise instance variables
        this.x = x;
        this.y = y;
        this.size = sizeOfNum;
        this.numbers = new ArrayList<Number>();
    }
    /**
     * Display zobrazuje cislo v parametri score
     * vdaka modulo cislo zobrayuje postupne -
     */
    public void showScore(int score) {
        int reminder;
        int workNum = score;
        int workX = this.x;
        int workY = this.y;
        this.clearDisplay();
        this.numbers.clear();
        //ak je mensie alebo rovne nule tak zobrazi 0
        if (workNum <= 0) {
            this.numbers.add(new Number(0, workX, workY, this.size));
        } else {
            while (workNum > 0) {
                reminder = workNum % 10;
                this.numbers.add(new Number(reminder, workX, workY, this.size));
                workNum = workNum / 10;
                workX -= (this.size + 2);
            }
        }
    }
    /**
     * Vycisti Display - znici jednotlive cisla a vycisti ArrayList
     */
    public void clearDisplay() {
        for (int i = 0; i < this.numbers.size(); i++) {
            this.numbers.get(i).destroyNum();
        }
        this.numbers.clear();
    }
}
