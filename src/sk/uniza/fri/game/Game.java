package sk.uniza.fri.game;

import sk.uniza.fri.extensions.Manazer;
import sk.uniza.fri.files.IncompatibleGameVersionException;
import sk.uniza.fri.map.GameMap;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 * 28/03/2022 - 08:19
 *
 * Trieda Game - trieda spajajuca vsetky triedy
 * - trieda vyuziva aj reuse kod z predchadzajucej hry: Attack on Pearl Harbor
 */
public class Game {
    private static Game gameInstance;

    private GameMap map;
    private String gamer;

    private int score;
    private int remainingBullets;

    private Manazer gameManager;

    private ButtonMeaning level;
    private GameState gameState;

    private MainPage mainPage;

    private Display displayScore;
    private Display displayBullets;

    //ukladanie a nacitanie suborov
    private static final String FILE_SAVE = "save.blizt";
    private static final String FILE_BEST_SCORE = "bestscore.txt";
    private static final int GAME_VERSION = 4;

    private boolean alreadyPlayed;

    /**
     * Konstruktor triedy Hra
     */
    private Game() {
        this.score = 0;

        String playerName;
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please write your name or nick: ");
        playerName = scanner.nextLine();
        //System.out.println(playerName);
        //System.out.println(playerName.length());
        if (playerName.length() == 0) {
            this.gamer = "PlayerOne";
        } else {
            this.gamer = playerName;
        }
        System.out.println("Your nick was set to: " + this.gamer);

        this.alreadyPlayed = false;
        this.gameManager = new Manazer();
        this.gameManager.spravujObjekt(this);

        //mainpage
        this.mainPage = new MainPage();
        this.gameManager = new Manazer();

        this.mainPage.showMenuPage();
        this.gameState = GameState.MENU;

        //display score
        int posXDisplay = 300;
        int posYDisplay = 20;
        int sizeOfDNum = 30;

        this.displayScore = new Display(posXDisplay, posYDisplay, sizeOfDNum);

        //display bullets
        posXDisplay -= 200;
        posYDisplay = 20;
        sizeOfDNum = 30;

        this.displayBullets = new Display(posXDisplay, posYDisplay, sizeOfDNum);

    }
    /**
     * Singleton - ziskanie jedinej instancie hry
     */
    public static Game getInstance() {
        //solving problem with Singleton programming solution
        if (Game.gameInstance == null) {
            Game.gameInstance = new Game();
        }
        return Game.gameInstance;
    }

    /**
     * Metoda start pre spustenie hry
     * @param level level podla ktoreho sa nastavia zaciatocne udaje
     */
    public void start(ButtonMeaning level) {

        this.mainPage.clearPage();
        this.gameState = GameState.PLAYING;
        System.out.println("Starting game...");
        this.level = level;
        //number of bullets and score
        this.score = 0;
        this.remainingBullets = this.level.getNumberOfBullets();

        this.displayScore.showScore(this.score);
        this.displayBullets.showScore(this.remainingBullets);

        int startPosMapX = 100;
        int startPosMapY = 100;
        this.map = new GameMap(50, startPosMapX, startPosMapY, 10, this.level.getNumberOfMines(), this.level.getNumberOfWall());
        //System.out.println("your playing");
        this.mainPage.showSaveButton();
    }

    /**
     * Metoda pre nastavenie koncovej stranky + funkcie s tym spojene
     */
    public void isEnd() {
        this.mainPage.hideSaveButton();
        this.map.clearGrid();
        this.gameState = GameState.END;
        this.mainPage.showEndPage();
        //this.gameManager.prestanSpravovatObjekt(this);
        //System.out.println("End");

    }

    /**
     * Pouzitie Manazera - posunutie dole
     */
    public void posunDole() {
        if (this.gameState == GameState.PLAYING) {
            if (this.map.moveTankDown()) {
                this.isEnd();
            }
        }
    }
    /**
     * Pouzitie Manazera - posunutie hore
     */
    public void posunHore() {
        if (this.gameState == GameState.PLAYING) {
            if (this.map.moveTankUp()) {
                this.isEnd();
            }
        }
    }
    /**
     * Pouzitie Manazera - posunutie vpravo
     */
    public void posunVpravo() {
        if (this.gameState == GameState.PLAYING) {
            if (this.map.moveTankRight()) {
                this.isEnd();
            }
        }
    }
    /**
     * Pouzitie Manazera - posunutie vlavo
     */
    public void posunVlavo() {
        if (this.gameState == GameState.PLAYING) {
            if (this.map.moveTankLeft()) {
                this.isEnd();
            }
        }
    }
    /**
     * Pouzitie Manazera - pouzitie mysky, funkcionalita sa rozdeluje podla stavu, v ktorom sa hra nachadza
     */
    public void vyberSuradnice(int x, int y) throws IOException {

        if (this.gameState == GameState.MENU) {

            if (this.mainPage.belongsToButton(x, y) == ButtonMeaning.EASY) {
                //System.out.println("you hit easy");

                this.start(ButtonMeaning.EASY);

            } else if (this.mainPage.belongsToButton(x, y) == ButtonMeaning.MEDIUM) {
                //System.out.println("you hit medium");

                this.start(ButtonMeaning.MEDIUM);

            } else if (this.mainPage.belongsToButton(x, y) == ButtonMeaning.HARD) {
                //System.out.println("you hit hard");
                this.start(ButtonMeaning.HARD);
            } else if (this.mainPage.belongsToButton(x, y) == ButtonMeaning.LOADGAME) {
                this.loadGame();
            }
        } else if (this.gameState == GameState.PLAYING) {
            //System.out.printf("suradnice: %d, %d \n", x, y);
            if (this.map.getIsEnd()) { //this.map.tankOrBunkerDestroyed()
                this.isEnd();
            } else {
                TankShot shotAt = this.map.tankShot(x, y);
                ButtonMeaning buttonMeaning = this.mainPage.belongsToButton(x, y);
                if (buttonMeaning == ButtonMeaning.SAVEGAME) {
                    this.saveGame(this.map);
                } else if (shotAt != null) {
                    this.remainingBullets -= shotAt.getMinusPoints();
                    this.score += shotAt.getPlusPoints();

                    this.displayBullets.showScore(this.remainingBullets);
                    this.displayScore.showScore(this.score);
                }
            }
            //konecna kontrola stavu municie
            if (this.remainingBullets <= 0) {
                System.out.println("No remaining bullets");
                this.isEnd();
            }

        } else if (this.gameState == GameState.END) {

            if (this.mainPage.belongsToButton(x, y) == ButtonMeaning.RESTART) {
                //System.out.println("you hit restart");
                this.writeBestScore();
                this.map.clearGrid();
                this.mainPage.clearPage();

                this.displayScore.clearDisplay();
                this.displayBullets.clearDisplay();
                this.alreadyPlayed = true;
                this.gameState = GameState.MENU;
                this.mainPage.showMenuPage();

            } else if (this.mainPage.belongsToButton(x, y) == ButtonMeaning.EXITGAME) {
                //System.out.println("you hit exit game");
                this.writeBestScore();
                this.gameManager.prestanSpravovatObjekt(this);
                System.exit(0);
            }
        }
    }
    //save and load

    /**
     * Metoda na ukladanie informacii do binarneho suboru
     * @param gameMap mapa, z ktorej sa informacie nacitaju
     */
    public void saveGame(GameMap gameMap) {
        // co budem ukladat - level, aktualna pozicia tanku, jeho skore, pozicia min, bunkru, ...
        System.out.println("Saving started");
        File saveFile = new File(FILE_SAVE);
        try (DataOutputStream save = new DataOutputStream(new FileOutputStream(saveFile))) {
            save.writeInt(GAME_VERSION); //pouzita na kontrolu kompatibility + ci uz save vznikol
            if (this.gamer != null) {
                save.writeUTF(this.gamer);
            } else {
                save.writeUTF("PlayerOne");
            }
            save.writeUTF(String.valueOf(this.level));
            save.writeInt(this.score);
            save.writeInt(this.remainingBullets);
            gameMap.saveInfo(save); // dlzka, akt.poz tanku, aktualne ifo o mape

            save.close();
            System.out.println("Save is done");
        } catch (IOException e) {
            System.out.println("File could not be created");
        }
    }

    /**
     * Metoda na nacitanie informacii z binarneho suboru
     */
    public void loadGame() {
        System.out.println("Loading started");
        File loadFile = new File(FILE_SAVE);
        try (DataInputStream load = new DataInputStream(new FileInputStream(loadFile))) {

            if (load.readInt() != GAME_VERSION) {
                throw new IncompatibleGameVersionException();
            }

            //name of gamer
            this.gamer = load.readUTF();
            this.level = ButtonMeaning.valueOf(load.readUTF());
            //score and remaining bullets
            this.score = load.readInt();
            this.remainingBullets = load.readInt();

            this.mainPage.clearPage();
            this.mainPage.showSaveButton();
            //loading game
            if (!this.alreadyPlayed) {
                int startPosMapX = 100;
                int startPosMapY = 100;

                this.map = new GameMap(50, startPosMapX, startPosMapY);
            }

            this.map.loadMapInfo(load);
            this.gameState = GameState.PLAYING;
            this.displayScore.showScore(this.score);
            this.displayBullets.showScore(this.remainingBullets);

            System.out.println("Load is done");
        } catch (FileNotFoundException e) {
            System.out.println("The load file does not exist");
        } catch (IOException e) {
            System.out.println("Error during loading the file");
        } catch (IncompatibleGameVersionException e) {
            System.out.println(e.getMessage());
        }

    }

    /**
     * Metoda na zapis najlepsieho skore a hraca
     * Reuse kodu z predchadzajucej hry: Attack on Pearl Harbor
     */
    public void writeBestScore() throws IOException {
        String bestPlayer = "";
        int bestScore = 0;
        try {
            File file = new File(FILE_BEST_SCORE);
            //first we have to read from the file than we can overwrite the content
            Scanner scanner = new Scanner(file);
            bestPlayer = scanner.nextLine();
            bestScore = scanner.nextInt();
            scanner.close();

            if (this.score > bestScore) {
                PrintWriter writer = new PrintWriter(file);
                writer.print(this.gamer + "\n" + this.score);
                writer.close();
                //writing the best score to the console
                System.out.println("The best player is: " + this.gamer + " with score: " + this.score);

            } else {
                System.out.println("The best player is: " + bestPlayer + " with score: " + bestScore);
            }
        } catch (IOException e) {
            System.out.println("Problem during file handling: " + FILE_BEST_SCORE);
        }


    }
}
