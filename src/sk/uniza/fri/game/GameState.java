package sk.uniza.fri.game;

/**
 * 28/03/2022 - 08:19
 * Enum pre stav hry - potrebny na odlisenie stavu v ktorom sa hra nachadza
 */
public enum GameState {
    MENU, PLAYING, END;
}
