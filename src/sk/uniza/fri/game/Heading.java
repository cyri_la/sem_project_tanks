package sk.uniza.fri.game;

/**
 * 28/03/2022 - 08:19
 *
 * Trieda Heading (Titulok), ktory je potomok OnPage
 */
public class Heading extends OnPage {
    /**
     * Konstruktor pre Heading - zobrazi nazov (obrazok) na danej x, y
     */
    public Heading(int x, int y, int sizeX, int sizeY, String file) {

        super(x, y, sizeX, sizeY, file);
    }
}
