package sk.uniza.fri.game;

import java.util.ArrayList;

/**
 * 28/03/2022 - 08:19
 *
 * Trieda Hlavna stranka
 * sluzi na zobrazenie menu, end page podla potrieb hry
 * optimalizovany kod z predchadzajucej hry: Attack on Pearl Harbor
 */
public class MainPage {
    private ArrayList<Button> buttons;
    private Heading heading;
    /**
     * Konstruktor pre triedu MainPage - hlavna stranka
     * obsahuje titulok a tlacidla
     */
    public MainPage() {
        // initialise instance variables
        int x = 300;
        int y = 200;
        int width = 200;
        int height = 100;
        this.buttons = new ArrayList<Button>();
        int gap = 50;
        this.heading = new Heading(x - width / 2, y - 2 * height, width * 2, height, "src/sk/uniza/fri/images/pages/heading.png");
        //
        this.buttons.add(new Button(x, y , width, height, "src/sk/uniza/fri/images/pages/easy.png", ButtonMeaning.EASY));
        this.buttons.add(new Button(x, y + height + gap , width, height, "src/sk/uniza/fri/images/pages/medium.png", ButtonMeaning.MEDIUM));
        this.buttons.add(new Button(x, y + 2 * height + 2 * gap , width, height, "src/sk/uniza/fri/images/pages/hard.png", ButtonMeaning.HARD));

        // load
        this.buttons.add(new Button(x + 2 * width, y - 1 * height, width, height, "src/sk/uniza/fri/images/pages/loadgame.png", ButtonMeaning.LOADGAME));

        // exit and restart
        this.buttons.add(new Button(x + 2 * width, y + 3 * height, width, height, "src/sk/uniza/fri/images/pages/restart.png", ButtonMeaning.RESTART));
        this.buttons.add(new Button(x + 2 * width, y + 4 * height, width, height, "src/sk/uniza/fri/images/pages/exitgame.png", ButtonMeaning.EXITGAME));
        // save - during the game
        this.buttons.add(new Button(x + 2 * width, y - 2 * height, width, height, "src/sk/uniza/fri/images/pages/savegame.png", ButtonMeaning.SAVEGAME));

    }
    /**
     * Metoda pre zobrazenie tlacidiel - easy, medium, hard, load + zobrazenie Heading-u
     */
    public void showMenuPage() {
        //making them visible
        for (int i = 0; i < this.buttons.size() - 3; i++) {
            this.buttons.get(i).showOnPage();
        }
        this.heading.showOnPage();
    }
    /**
     * Metoda pre zobrazenie tlacidiel - restart a exit
     */
    public void showEndPage() {
        //
        for (int i = this.buttons.size() - 3 ; i < this.buttons.size() - 1; i++) {
            this.buttons.get(i).showOnPage();
        }

    }
    /**
     * Metoda pre skrytie tlacidiel a Heading-u
     */
    public void clearPage() {

        for (int i = 0; i < this.buttons.size(); i++) {
            this.buttons.get(i).hideOnPage();
        }
        this.heading.hideOnPage();
    }
    /**
     * Na zaklade x, y zisti ci je to bod patriaci nejakemu tlacidlu
     * ak ano vrati vyznam tlacidla
     */
    public ButtonMeaning belongsToButton(int x, int y) {
        for (int i = 0; i < this.buttons.size(); i++) {
            if (((this.buttons.get(i).getCoordX() < x ) && (this.buttons.get(i).getCoordX() + this.buttons.get(i).getSizeX() > x )) && ((this.buttons.get(i).getCoordY() < y ) && (this.buttons.get(i).getCoordY() + this.buttons.get(i).getSizeY() > y ))) {
                return this.buttons.get(i).getButtonMeaning();
            }
        }
        return null;
    }

    /**
     * Metoda pre zobrazenie tlacidla Save
     */
    public void showSaveButton() {

        this.buttons.get(this.buttons.size() - 1).showOnPage();
    }

    /**
     * Metoda pre skrytie tlacidla Save
     */

    public void hideSaveButton() {
        this.buttons.get(this.buttons.size() - 1).hideOnPage();
    }
}
