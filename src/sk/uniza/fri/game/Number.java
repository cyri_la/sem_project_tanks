package sk.uniza.fri.game;

import sk.uniza.fri.extensions.Obrazok;

/**
 * 28/03/2022 - 08:19
 * Trieda Number (Cislo) zobrazi obrazok cisla
 * optimalizovane pre potreby Blitzkreig z predchadajucej hry: Attacj on Pearl Harbor
 */
public class Number {
    private int num;
    private Obrazok picture;
    /**
     * Konstruktor pre triedu Number
     */
    public Number(int num, int x, int y, int size) {
        // initialise instance variables
        this.num = num;
        this.picture = new Obrazok("src/sk/uniza/fri/images/display/0.png");
        this.picture.zmenVelkost(size, size);
        this.picture.zmenPolohu(x, y);
        this.showNumber();
    }
    /**
     * Zobrazi obrazok, podla atributu num
     */
    public void showNumber() {
        String fileDirection = "src/sk/uniza/fri/images/display/";
        switch (this.num) {
            case 0:
                fileDirection += "0.png";
                break;
            case 1:
                fileDirection += "1.png";
                break;
            case 2:
                fileDirection += "2.png";
                break;
            case 3:
                fileDirection += "3.png";
                break;
            case 4:
                fileDirection += "4.png";
                break;
            case 5:
                fileDirection += "5.png";
                break;
            case 6:
                fileDirection += "6.png";
                break;
            case 7:
                fileDirection += "7.png";
                break;
            case 8:
                fileDirection += "8.png";
                break;
            case 9:
                fileDirection += "9.png";
                break;
            default:
                System.out.println("Error");
        }
        this.picture.zmenObrazok(fileDirection);
        this.picture.zobraz();
    }
    /**
     * Metoda odstrani cislo - skryje obrazok
     */
    public void destroyNum() {

        this.picture.skry();
    }
}
