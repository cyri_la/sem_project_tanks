package sk.uniza.fri.game;

import sk.uniza.fri.extensions.Obrazok;

/**
 * 28/03/2022 - 08:19
 *
 * Trieda OnPage vznikla ako optimalizacia spolocnich vlastnosti triedy Button a Heading
 */
public class OnPage {
    private int coordX;
    private int coordY;
    private int sizeX;
    private int sizeY;
    private Obrazok picture;

    /**
     * Konstruktor tiedy OnPage
     * @param x HL bod
     * @param y HL bod
     * @param sizeX velkost v x suradnici
     * @param sizeY velkost v y suradnici
     * @param file subor obrazka
     */

    public OnPage(int x, int y, int sizeX, int sizeY, String file) {
        this.coordX = x;
        this.coordY = y;
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.picture = new Obrazok(file);

        this.picture.zmenVelkost(sizeX, sizeY);
        this.picture.zmenPolohu(x + (sizeX / 2), y + (sizeY / 2));
    }
    /**
     * Zmeni suradnice - setter pre x, y a nastavi obrazok na dane suradnice
     */
    public void changeCoords(int x, int y) {
        this.coordX = x;
        this.coordY = y;
        this.picture.zmenPolohu(x - this.sizeX , y - this.sizeY);
    }
    /**
     * Zmeni velkosti v x, y suardnici - setter pre sizeX, sizeY a nastavi obrazok na danu velkost
     */
    public void changeSize(int sizeX, int sizeY) {
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.picture.zmenVelkost(sizeX, sizeY);
    }
    /**
     * Getter pre suradnicu x
     */
    public int getCoordX() {

        return this.coordX;
    }
    /**
     * Getter pre suradnicu y
     */
    public int getCoordY() {

        return this.coordY;
    }
    /**
     * Getter pre sizeX
     */
    public int getSizeX() {

        return this.sizeX;
    }
    /**
     * Getter pre sizeY
     */
    public int getSizeY() {

        return this.sizeY;
    }
    /**
     * Zobrazi objekt pomocou obrazku
     */
    public void showOnPage() {

        this.picture.zobraz();
    }
    /**
     * Skryje objekt pomocou obrazku
     */
    public void hideOnPage() {

        this.picture.skry();
    }
}
