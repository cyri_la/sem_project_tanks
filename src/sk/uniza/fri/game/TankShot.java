package sk.uniza.fri.game;

/**
 * 28/03/2022 - 08:19
 *
 * Enum TankShot - reprezentuje moznosti ktore moze zasiahnut tank
 */
public enum TankShot {
    MINE(5, 2), BUNKER(10, 5), WALL(2, 2), GROUND(0, 1);

    private int plusPoints;
    private int minusPoints;

    /**
     *
     * @param plusPoints plusove body do skore po zasiahnuti bloku
     * @param minusPoints minusove body, ktore sa odpocitaju z municie po zasiahnuti bloku
     */
    TankShot(int plusPoints, int minusPoints) {
        this.plusPoints = plusPoints; //score
        this.minusPoints = minusPoints; //numofBullets
    }

    /**
     * Getter pre plusPoints
     */
    public int getPlusPoints() {
        return this.plusPoints;
    }
    /**
     * Getter pre minusPoints
     */
    public int getMinusPoints() {
        return this.minusPoints;
    }
}
