package sk.uniza.fri.map;

import sk.uniza.fri.extensions.Obrazok;

/**
 * 28/03/2022 - 08:19
 *
 * Trieda Block - predstavuje blok v sieti
 * predok pre triedy Bunker, Tank, Wall, Mine
 */
public class Block {
    private Obrazok picture;
    private int x;
    private int y;
    private int size;
    private String imageName;
    private boolean wasHit;

    /**
     * Konstruktor pre triedu Block
     * @param coorX x suradnica
     * @param coorY y suradnica
     * @param imageName nazov obrazku
     * @param size velkost obrazku
     */
    public Block(int coorX, int coorY, String imageName, int size ) {
        this.x = coorX;
        this.y = coorY;
        this.size = size;
        this.picture = new Obrazok("src/sk/uniza/fri/images/" + imageName + ".png");

        this.imageName = imageName;
        this.picture.zmenVelkost(this.size, this.size);
        this.picture.zmenPolohu(this.x + this.size / 2, this.y + this.size / 2);
        this.wasHit = false;
        this.picture.zobraz();
    }

    /**
     * Metoda na posun bloku
     * @param coorX nova suradnica x
     * @param coorY nova suradnica y
     */
    public void moveBlock(int coorX, int coorY) {
        //System.out.println("moveBlock method");
        this.picture.skry();
        this.picture.zmenPolohu(coorX + this.size / 2, coorY + this.size / 2);
        this.x = coorX;
        this.y = coorY;
        this.picture.zobraz();
    }

    /**
     * Metoda na nastavenie atributu wasHit, blok sa zmeni po triafeni na ground
     */
    public void wasHit() {
        this.wasHit = true;
        this.imageName = "ground";
        this.picture.zmenObrazok("src/sk/uniza/fri/images/ground.png"); //nazov suboru s ground obrazkom

    }

    /**
     * Metoda zobrazi blok
     */
    public void showBlock() {

        this.picture.zobraz();
    }

    /**
     * Metoda skryje blok
     */
    public void hideBlock() {

        this.picture.skry();
    }

    /**
     * Getter pre imageName - nazov obrazku
     */
    public String getImageName() {

        return this.imageName;
    }

    /**
     * Metoda na zmenu obrazku
     * @param imageName nazov obrazku
     */
    public void changePicture(String imageName) {

        this.picture.zmenObrazok("src/sk/uniza/fri/images/" + imageName + ".png");
    }

    /**
     * Setter pre imageName
     * @param imageName novy nazov obrazku
     */
    protected void setImageName(String imageName) {

        this.imageName = imageName;
    }

    /**
     * Getter pre suardnicu x
     */
    public int getX() {

        return this.x;
    }
    /**
     * Getter pre suardnicu y
     */
    public int getY() {

        return this.y;
    }

    /**
     * Setter pre suardnicu x
     */
    public void setX(int x) {

        this.x = x;
    }
    /**
     * Setter pre suardnicu y
     */
    public void setY(int y) {

        this.y = y;
    }
    /**
     * Getter pre wasHit
     */
    public boolean getWasHit() {

        return this.wasHit;
    }
}
