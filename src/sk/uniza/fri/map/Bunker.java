package sk.uniza.fri.map;

/**
 * 28/03/2022 - 08:19
 *
 * Trieda Bunker - predstavueje bunker - ciel znicenia
 * potomok triedy Block
 */
public class Bunker extends Block {
    private int hp;
    private int damage;

    /**
     * Konstruktor Bunker
     * @param coordX suradnica x
     * @param coordY suradnica y
     * @param sizeOfBlock velkost bloku
     * @param hp health points pre Bunker
     */
    public Bunker(int coordX, int coordY, int sizeOfBlock, int hp) {
        super(coordX, coordY, "bunker", sizeOfBlock);
        this.hp = hp;
        this.damage = 20;
    }

    /**
     * Metoda wasHit prepisana - zmeni nazov obrazku na bunker
     */
    @Override
    public void wasHit() {
        //super.setImageName("bunker");
    }

    /**
     * Metoda na znizenie hp
     * @param damage sila utoku nepriatela
     * @return vrati true, alebo false - true ak je hp <= ako 0
     */
    public boolean lowerHp(int damage) {
        System.out.println("Bunker is being attacked with damage: " + damage);
        if (this.hp - damage > 0) {
            this.hp -= damage;
            return false;
        } else {
            System.out.println("Bunker is destroyed");
            return true;
        }
    }

    /**
     * Getter pre damage
     */
    public int getDamage() {

        return this.damage;
    }
    /**
     * Getter pre hp
     */
    public int getHP() {

        return this.hp;
    }
    /**
     * Setter pre hp
     */
    public void setHP(int hp) {

        this.hp = hp;
    }
}
