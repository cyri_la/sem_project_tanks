package sk.uniza.fri.map;

import sk.uniza.fri.files.BlockSignature;
import sk.uniza.fri.game.TankShot;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Random;

import static sk.uniza.fri.files.BlockSignature.MINE;

/**
 * 28/03/2022 - 08:19
 *
 * Trieda GameMap - mapa pre hru, obsahuje hernu plochu a tank
 */
public class GameMap {
    private Block[][] grid;
    private int sizeOfBlock;
    private int startX;
    private int startY;
    private int numOfBlocks;
    private Random randGenerator;
    private int numberOfMines;
    private int numberOfWall;
    private int actPosX;
    private int actPosY;

    private int actPosXOnGrid;
    private int actPosYOnGrid;
    private int gap;
    private Tank tank;

    private int[] keyHit;
    private boolean isEnd;
    private boolean tankOrBunkerDestroyed;

    /**
     * Konstruktor pre triedu GameMap
     * @param sizeOfBlock velkost bloku
     * @param startX zacinajuca suradnica x mriezky
     * @param startY zacinajuca suradnica y mriezky
     * @param numOfBlocks pocet blokov - mriezka n * n
     * @param numberOfMines pocet min v ploche
     * @param numberOfWall pocet stien v ploche
     */
    public GameMap(int sizeOfBlock, int startX, int startY, int numOfBlocks, int numberOfMines, int numberOfWall) {

        this.numberOfMines = numberOfMines;
        this.numberOfWall = numberOfWall;

        this.tankOrBunkerDestroyed = false;

        this.isEnd = false;
        this.keyHit = new int[] {0, 0, 0, 0};
        this.sizeOfBlock = sizeOfBlock;
        this.startX = startX;
        this.startY = startY;
        this.numOfBlocks = numOfBlocks;
        this.grid = new Block[this.numOfBlocks][this.numOfBlocks];
        this.randGenerator = new Random();
        this.gap = 5;
        //map construction
        this.mapConstruction(startX, startY);
        this.generateMap(this.numberOfMines, this.numberOfWall);
    }

    /**
     * Konstruktor pre triedu Gamemap pri loadovani mapy prvykrat
     * @param sizeOfBlock velkost bloku
     * @param startX zacinajuca suradnica x
     * @param startY zacinajuca suradnica y
     */
    public GameMap(int sizeOfBlock, int startX, int startY) {
        // contructor for loaded gameMap
        this.tankOrBunkerDestroyed = false;

        this.isEnd = false;
        this.keyHit = new int[] {0, 0, 0, 0};
        this.sizeOfBlock = sizeOfBlock;

        this.startX = startX;
        this.startY = startY;

        this.randGenerator = new Random();
        this.gap = 5;
    }

    /**
     * Konstrukcia mapy
     * @param startX zacinajuca suradnica x
     * @param startY zacinajuca suradnica y
     * vytvori mriezku blokov - Ground
     */
    private void mapConstruction(int startX, int startY) {
        System.out.println("Constructing map...");
        int x;
        int y = startY;
        for (int i = 0; i < this.numOfBlocks; i++) {
            x = startX;
            for (int j = 0; j < this.numOfBlocks; j++) { //this.startX + this.sizeOfBlock * (j + 1), this.startY + this.sizeOfBlock * (i + 1),
                this.grid[i][j] = new Ground(x, y, this.sizeOfBlock);
                x += this.sizeOfBlock + this.gap;
            }
            y += this.sizeOfBlock + this.gap;
        }
    }

    /**
     * Metoda na nahodne generovanie mapy
     * @param numberOfMines pocet min
     * @param numberOfWall pocet stien
     */
    public void generateMap(int numberOfMines, int numberOfWall) {

        this.generateBunkerPosition();
        for (int i = 0; i < numberOfMines; i++) {
            this.generateMinePosition();
        }
        for (int i = 0; i < numberOfWall; i++) {
            this.generateWallPosition();
        }
        this.generateTankPosition();


    }

    /**
     * Metoda na generovanie pozicie miny
     */
    private void generateMinePosition() {
        int randPosI;
        int randPosJ;
        do {
            randPosI = this.randGenerator.nextInt(this.numOfBlocks);
            randPosJ = this.randGenerator.nextInt(this.numOfBlocks);
        } while (!(this.grid[randPosI][randPosJ].getImageName().equals("ground")));
        //System.out.printf("Mine pozicia: %d, %d \n", randPosI, randPosJ);
        //musime najprv skryt povodny obrazok
        this.grid[randPosI][randPosJ].hideBlock();
        this.grid[randPosI][randPosJ] = new Mine(this.grid[randPosI][randPosJ].getX(), this.grid[randPosI][randPosJ].getY(), this.sizeOfBlock);

    }
    /**
     * Metoda na generovanie pozicie steny
     */
    private void generateWallPosition() {
        int randPosI;
        int randPosJ;
        do {
            randPosI = this.randGenerator.nextInt(this.numOfBlocks);
            randPosJ = this.randGenerator.nextInt(this.numOfBlocks);
        } while (!(this.grid[randPosI][randPosJ].getImageName().equals("ground")));
        this.grid[randPosI][randPosJ].hideBlock();
        this.grid[randPosI][randPosJ] = new Wall(this.grid[randPosI][randPosJ].getX(), this.grid[randPosI][randPosJ].getY(), this.sizeOfBlock);

    }
    /**
     * Metoda na generovanie pozicie bunkru
     */
    private void generateBunkerPosition() {
        int randPosI;
        int randPosJ;
        do {
            randPosI = this.randGenerator.nextInt(this.numOfBlocks);
            randPosJ = this.randGenerator.nextInt(this.numOfBlocks);
        } while (!(this.grid[randPosI][randPosJ].getImageName().equals("ground")));
        this.grid[randPosI][randPosJ].hideBlock();
        this.grid[randPosI][randPosJ] = new Bunker(this.grid[randPosI][randPosJ].getX(), this.grid[randPosI][randPosJ].getY(), this.sizeOfBlock, 100);

    }
    /**
     * Metoda na generovanie pozicie Tanku
     */
    private void generateTankPosition() {
        int randPosI;
        int randPosJ;
        do {
            randPosI = this.randGenerator.nextInt(this.numOfBlocks);
            randPosJ = this.randGenerator.nextInt(this.numOfBlocks);
        } while (!(this.grid[randPosI][randPosJ].getImageName().equals("ground")));
        //System.out.printf("Pozicia tanku: %d, %d \n", randPosI, randPosJ);

        this.tank = new Tank(this.grid[randPosI][randPosJ].getX(), this.grid[randPosI][randPosJ].getY(), this.sizeOfBlock, 100, 5);
        this.actPosX = this.startX + randPosJ * this.sizeOfBlock;
        this.actPosY = this.startY + randPosI * this.sizeOfBlock;
        this.actPosXOnGrid = randPosJ;
        this.actPosYOnGrid = randPosI;
    }

    /**
     * Metoda na posun tanku po mriezke
     * @return boolean hodnotu true, false - true ak nasiel na minu
     */
    public boolean moveTankDown() {

        this.keyHit[0]++;
        this.keyHit[1] = 0;
        this.keyHit[2] = 0;
        this.keyHit[3] = 0;

        if (this.keyHit[0] == 1) {
            this.tank.changePicture("s");
        } else {
            boolean boundaries = this.checkBoundaries(0, 0, this.numOfBlocks, this.numOfBlocks, this.actPosXOnGrid, this.actPosYOnGrid + 1);
            boolean isGround = this.grid[this.actPosYOnGrid + 1][this.actPosXOnGrid].getImageName().equals("ground");
            boolean isMine = this.grid[this.actPosYOnGrid + 1][this.actPosXOnGrid].getImageName().equals("mine");
            //System.out.println(isGround);

            if (boundaries && (isGround || isMine)) {
                this.tank.moveBlock(this.grid[this.actPosYOnGrid + 1][this.actPosXOnGrid].getX(), this.grid[this.actPosYOnGrid + 1][this.actPosXOnGrid].getY());
                this.actPosY = this.actPosY + this.sizeOfBlock + this.gap;
                this.actPosYOnGrid += 1;
                this.tank.setY(this.actPosY);

                if (isMine) {
                    Block mine  = this.grid[this.actPosYOnGrid][this.actPosXOnGrid];
                    if ((mine instanceof Mine)) {
                        if (!mine.getWasHit()) {
                            ((Mine)mine).wasRunOver();
                            //System.out.println("bum");
                            this.isEnd = true;
                        }
                    }

                }
                //System.out.printf("Pozicia tanku: %d, %d \n", this.actPosX, this.actPosY);
            }
        }
        return this.isEnd;

    }
    /**
     * Metoda na posun tanku po mriezke
     * @return boolean hodnotu true, false - true ak nasiel na minu
     */
    public boolean moveTankUp() {
        this.keyHit[1]++;
        this.keyHit[0] = 0;
        this.keyHit[2] = 0;
        this.keyHit[3] = 0;


        if (this.keyHit[1] == 1) {
            this.tank.changePicture("w");
        } else {
            boolean boundaries = this.checkBoundaries(0, 0, this.numOfBlocks, this.numOfBlocks, this.actPosXOnGrid, this.actPosYOnGrid - 1);
            boolean isGround = this.grid[this.actPosYOnGrid - 1][this.actPosXOnGrid].getImageName().equals("ground");
            boolean isMine = this.grid[this.actPosYOnGrid - 1][this.actPosXOnGrid].getImageName().equals("mine");

            if (boundaries && (isGround || isMine)) {
                this.tank.moveBlock(this.grid[this.actPosYOnGrid - 1][this.actPosXOnGrid].getX(), this.grid[this.actPosYOnGrid - 1][this.actPosXOnGrid].getY());
                this.actPosY = this.actPosY - this.sizeOfBlock - this.gap;
                this.actPosYOnGrid -= 1;
                this.tank.setY(this.actPosY);
                Block mine  = this.grid[this.actPosYOnGrid][this.actPosXOnGrid];

                if ((mine instanceof Mine)) {
                    if (!mine.getWasHit()) {
                        ((Mine)mine).wasRunOver();
                        //System.out.println("bum");
                        this.isEnd = true;
                    }
                }
                //System.out.printf("Pozicia tanku: %d, %d \n", this.actPosX, this.actPosY);
            }
        }
        return this.isEnd;
    }
    /**
     * Metoda na posun tanku po mriezke
     * @return boolean hodnotu true, false - true ak nasiel na minu
     */
    public boolean moveTankLeft() {
        this.keyHit[2]++;
        this.keyHit[1] = 0;
        this.keyHit[0] = 0;
        this.keyHit[3] = 0;


        if (this.keyHit[2] == 1) {
            this.tank.changePicture("a");
        } else {
            boolean boundaries = this.checkBoundaries(0, 0, this.numOfBlocks, this.numOfBlocks, this.actPosXOnGrid - 1, this.actPosYOnGrid);
            boolean isGround = this.grid[this.actPosYOnGrid][this.actPosXOnGrid - 1].getImageName().equals("ground");
            boolean isMine = this.grid[this.actPosYOnGrid][this.actPosXOnGrid - 1].getImageName().equals("mine");

            if (boundaries && (isGround || isMine)) {
                this.tank.moveBlock(this.grid[this.actPosYOnGrid][this.actPosXOnGrid - 1].getX(), this.grid[this.actPosYOnGrid][this.actPosXOnGrid - 1].getY());
                this.actPosX = this.actPosX - this.sizeOfBlock - this.gap;
                this.actPosXOnGrid -= 1;
                this.tank.setX(this.actPosX);
                Block mine  = this.grid[this.actPosYOnGrid][this.actPosXOnGrid];

                if ((mine instanceof Mine)) {
                    if (!mine.getWasHit()) {
                        ((Mine)mine).wasRunOver();
                        //System.out.println("bum");
                        this.isEnd = true;
                    }
                }
                //System.out.printf("Pozicia tanku: %d, %d \n", this.actPosX, this.actPosY);
            }
        }
        return this.isEnd;
    }
    /**
     * Metoda na posun tanku po mriezke
     * @return boolean hodnotu true, false - true ak nasiel na minu
     */
    public boolean moveTankRight() {
        this.keyHit[3]++;
        this.keyHit[1] = 0;
        this.keyHit[2] = 0;
        this.keyHit[0] = 0;

        if (this.keyHit[3] == 1) {
            this.tank.changePicture("d");
        } else {
            boolean boundaries = this.checkBoundaries(0, 0, this.numOfBlocks, this.numOfBlocks, this.actPosXOnGrid + 1, this.actPosYOnGrid);
            boolean isGround = this.grid[this.actPosYOnGrid][this.actPosXOnGrid + 1].getImageName().equals("ground");
            boolean isMine = this.grid[this.actPosYOnGrid][this.actPosXOnGrid + 1].getImageName().equals("mine");

            if (boundaries && (isGround || isMine) )  {
                this.tank.moveBlock(this.grid[this.actPosYOnGrid][this.actPosXOnGrid + 1].getX(), this.grid[this.actPosYOnGrid][this.actPosXOnGrid + 1].getY());
                this.actPosX = this.actPosX + this.sizeOfBlock + this.gap;
                this.actPosXOnGrid += 1;
                this.tank.setX(this.actPosX);
                Block mine  = this.grid[this.actPosYOnGrid][this.actPosXOnGrid];

                if ((mine instanceof Mine)) {
                    if (!mine.getWasHit()) {
                        ((Mine)mine).wasRunOver();
                        //System.out.println("bum");
                        this.isEnd = true;
                    }
                }
                //System.out.printf("Pozicia tanku: %d, %d \n", this.actPosX, this.actPosY);
            }
        }
        return this.isEnd;
    }

    /**
     * Metoda na kontrolu okrajov
     * @param lowerBoundaryX dolna hranica x
     * @param lowerBoundaryY dolna hranica y
     * @param upperBoundaryX horna hranica x
     * @param upperBoundaryY dolna hranica y
     * @param actPosX aktulna pozicia x
     * @param actPosY aktualna pozicia y
     * @return vracia true, false ak splna poddmienky
     */
    private boolean checkBoundaries(int lowerBoundaryX, int lowerBoundaryY, int upperBoundaryX, int upperBoundaryY, int actPosX, int actPosY) {
        return ((lowerBoundaryX <= actPosX) && (actPosX < upperBoundaryX) && (lowerBoundaryY <= actPosY) && (actPosY < upperBoundaryY));
    }

    /**
     * Metoda tankShot - na aky typ bloku strelil tank
     * @param x x suradnica mysi
     * @param y y suradnica mysi
     * @return vrati enum podla typu
     */
    public TankShot tankShot(int x, int y) {
        Block block = this.findBlock(x, y);
        if (block != null) {
            boolean blockWasHit = block.getWasHit();
            block.wasHit();
            this.tank.changeDamage(1);
            if (block instanceof Bunker) { //block instanceof Bunker nefungovalo preco ?
                //System.out.println("Je bunker");
                this.isEnd = (this.tank.lowerHp(((Bunker)block).getDamage()) || ((Bunker)block).lowerHp(this.tank.getDamage()));
                return TankShot.BUNKER;
            } else if (block instanceof Ground) {
                //System.out.println("Je ground");
                return TankShot.GROUND;
            } else if (block instanceof Mine) {
                //System.out.println("Je mina");
                if (blockWasHit) {
                    //System.out.println("uz bola trafena");
                    return TankShot.GROUND;
                }
                return TankShot.MINE;
            }  else if (block instanceof Wall) {
                //System.out.println("Je wall");
                if (blockWasHit) {
                   // System.out.println("uz bola trafena");
                    return TankShot.GROUND;
                }
                return TankShot.WALL;
            }
        }
        return null;

    }

    /**
     * Metoda findBlock - najde blok podla x, y
     * @param x x suradnica
     * @param y y suradnica
     * @return vrati blok z mriezky, ktoremu patri x a y
     */
    private Block findBlock(int x, int y) {
        int range = this.tank.getArtilleryRange();
        Block choosen;
        for (int i = 0; i < this.numOfBlocks; i++) {
            for (int j = 0; j < this.numOfBlocks; j++) {

                choosen = this.grid[i][j];

                if (((choosen.getX() <= x) && (x <= (choosen.getX() + this.sizeOfBlock))) && ((choosen.getY() <= y) && (y <= (choosen.getY() + this.sizeOfBlock)))) {
                    //System.out.printf("choosen: %d, %d \n", choosen.getX(), choosen.getY());
                    //checking artillery range
                    if (((Math.abs( i - this.actPosYOnGrid ) <= range) && (this.actPosXOnGrid == j)) || ((Math.abs( j - this.actPosXOnGrid ) <= range) && (this.actPosYOnGrid == i))) {
                        //ak strielame na poziciu tanku vrati null aby sa nezmenil obrazok
                        if ((i == this.actPosYOnGrid ) && (j == this.actPosXOnGrid)) {
                            return null;
                        } else {
                            return choosen;
                        }
                    }
                }
            }
        }
        return null;
    }

    /**
     * Getter pre atribut isEnd
     */
    public boolean getIsEnd() {
        return this.isEnd;
    }

    /**
     * Metoda pre vycistenie mriezky
     */
    public void clearGrid() {
        for (int i = 0; i < this.numOfBlocks; i++) {
            for (int j = 0; j < this.numOfBlocks; j++) {
                this.grid[i][j].hideBlock();
            }
        }
        this.tank.hideBlock();
    }

    /**
     * Metoda na zobrazenie mriezky
     */
    public void showGrid() {
        for (int i = 0; i < this.numOfBlocks; i++) {
            for (int j = 0; j < this.numOfBlocks; j++) {
                this.grid[i][j].showBlock();
            }
        }
        this.tank.showBlock();
    }

    /**
     * Metoda na ulozenie informacii o mape
     * @param save save subor do ktoreho ukladame
     */
    public void saveInfo(DataOutputStream save) throws IOException {
        save.writeInt(this.numOfBlocks);//dlzka mapy
        //aktualna pozicia tanku x, y na mriezke
        save.writeInt(this.actPosXOnGrid);
        save.writeInt(this.actPosYOnGrid);
        //hp of tank
        save.writeInt(this.tank.getHp());
        save.writeInt(this.tank.getDamage());
        //zapis mapy
        String map = "";
        int hpBunker = 100;
        for (int i = 0; i < this.numOfBlocks; i++) {
            for (int j = 0; j < this.numOfBlocks; j++) {
                Block block = this.grid[i][j];

                if (block instanceof Mine) {
                    if (((Mine)block).getWasHit()) {
                        map += BlockSignature.MINE_DISABLED.getSignature();
                    } else {
                        map += MINE.getSignature();
                    }
                } else if (block instanceof Ground) {
                    map += BlockSignature.GROUND.getSignature();
                } else if (block instanceof Wall) {
                    if (block.getWasHit()) {
                        map += BlockSignature.WALL_DISABLED.getSignature();
                    } else {
                        map += BlockSignature.WALL.getSignature();
                    }
                } else if (block instanceof Bunker) {
                    map += BlockSignature.BUNKER.getSignature(); //zapise hodnotu hp akonahle najde Bunker
                    hpBunker = ((Bunker)block).getHP();
                }
                map += " ";
            }
        }
        //System.out.println(map);
        save.writeUTF(map);
        //ulozi hp bunkru
        save.writeInt(hpBunker);
        //System.out.println("Save is ok");

    }

    /**
     * Metoda nacita informacie o mape
     * @param load subor z ktoreho sa nacitaju informacie
     */
    public void loadMapInfo(DataInputStream load) throws IOException {
        System.out.println("Loading info about the map...");
        this.numOfBlocks = load.readInt();
        this.actPosXOnGrid = load.readInt();
        this.actPosYOnGrid = load.readInt();
        int hpTank = load.readInt();
        int damageTank = load.readInt();
        // generovanie mapy na zaklade loadu
        this.grid = new Block[this.numOfBlocks][this.numOfBlocks];
        this.mapConstruction(this.startX, this.startY);

        this.actPosX = this.grid[this.actPosYOnGrid][this.actPosXOnGrid].getX();
        this.actPosY = this.grid[this.actPosYOnGrid][this.actPosXOnGrid].getY();
        //something wrong here
        this.tank = new Tank(this.actPosX, this.actPosY, this.sizeOfBlock, hpTank, damageTank);

        int posIBunker = 0;
        int posJBunker = 0;
        String text = load.readUTF();
        String[] letters = text.split(" ");
        //ladiace vypisy
        //System.out.println(text);
        //System.out.println(letters);

        System.out.println("Loading map...");
        int counterForLetters = 0;
        for (int i = 0; i < this.numOfBlocks; i++) {
            for (int j = 0; j < this.numOfBlocks; j++) {

                //System.out.println(letters[i + j]);
                switch (letters[counterForLetters]) {
                    case "m":
                        //System.out.println("creating mine");
                        this.grid[i][j].hideBlock();
                        this.grid[i][j] = new Mine(this.grid[i][j].getX(), this.grid[i][j].getY(), this.sizeOfBlock);
                        break;
                    case "g": //preskoci lebo vsetky su defaultne nastavene na ground
                        //System.out.println("creating ground");
                        break;
                    case "d":
                        //System.out.println("creating disabled mine");
                        this.grid[i][j].hideBlock();
                        this.grid[i][j] = new Mine(this.grid[i][j].getX(), this.grid[i][j].getY(), this.sizeOfBlock);
                        this.grid[i][j].wasHit();
                        break;
                    case "b":
                        //System.out.println("creating bunker");
                        this.grid[i][j].hideBlock();
                        this.grid[i][j] = new Bunker(this.grid[i][j].getX(), this.grid[i][j].getY(), this.sizeOfBlock, 100); //hodnota hp sa zmeni
                        posIBunker = i;
                        posJBunker = j;
                        break;
                    case "w":
                        //System.out.println("creating wall");
                        this.grid[i][j].hideBlock();
                        this.grid[i][j] = new Wall(this.grid[i][j].getX(), this.grid[i][j].getY(), this.sizeOfBlock); //hodnota hp sa zmeni
                        break;
                    case "a":
                        //System.out.println("creating hit wall");
                        this.grid[i][j].hideBlock();
                        this.grid[i][j] = new Wall(this.grid[i][j].getX(), this.grid[i][j].getY(), this.sizeOfBlock); //hodnota hp sa zmeni
                        this.grid[i][j].wasHit();
                        break;
                    default:
                        throw new IllegalStateException("Unexpected value: " + load.readUTF());
                }
                counterForLetters++;
            }
        }
        //zmena hpBunker
        Block block = this.grid[posIBunker][posJBunker];
        if (block instanceof Bunker) {
            ((Bunker)block).setHP(load.readInt());
        }

        this.showGrid();
    }

}
