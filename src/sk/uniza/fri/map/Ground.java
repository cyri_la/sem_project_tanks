package sk.uniza.fri.map;

/**
 * 28/03/2022 - 08:19
 *
 * Trieda Ground - zem
 * potomok triedy Block
 */
public class Ground extends Block {
    /**
     * Konstruktor triedy Ground
     * @param coorX suradnica x
     * @param coorY suradnica y
     * @param sizeOfBlock velkost bloku
     */
    public Ground(int coorX, int coorY, int sizeOfBlock) {

        super(coorX, coorY, "ground", sizeOfBlock);
    }
}
