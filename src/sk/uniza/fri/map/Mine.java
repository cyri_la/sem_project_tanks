package sk.uniza.fri.map;

/**
 * 28/03/2022 - 08:19
 *
 * Trieda Mine - mina
 * potomok triedy Block
 */
public class Mine extends Block {

    private boolean wasRunOver;
    /**
     * Konstruktor triedy Mine
     * @param coorX suradnica x
     * @param coorY suradnica y
     * @param sizeOfBlock velkost bloku
     */
    public Mine(int coorX, int coorY, int sizeOfBlock) {
        super(coorX, coorY, "ground", sizeOfBlock);
        super.setImageName("mine");
        this.wasRunOver = false;
    }
    /**
     * Metoda wasHit prepisana - zmeni nazov obrazku na mine, ale obrazok sa zmeni na ground pomocou super
     */
    @Override
    public void wasHit() {
        super.wasHit();
        this.changePicture("mine");
    }

    /**
     * Setter pre wasRuunOver
     * metoda zavedená do bududcna
     */
    public void setWasRunOver(boolean wasRunOver) {

        this.wasRunOver = wasRunOver;
    }
    /**
     * Getter pre wasRuunOver
     * metoda zavedená do bududcna
     */
    public boolean getWasRunOver() {

        return this.wasRunOver;
    }
    /**
     * Metoda wasRunOver - cez minu sme presli - nastavi atribut wasRunOver na true a zmeni obrazok
     */
    public void wasRunOver() {
        this.wasRunOver = true;
        System.out.println("You run over mine");
        this.changePicture("mine");
    }
}
