package sk.uniza.fri.map;

/**
 * 28/03/2022 - 08:19
 *
 * @author krist
 */
public class Tank extends Block {
    private int artilleryRange;
    private int hp;
    private int damage;
    private boolean isDestroyed;

    /**
     * Konstruktor pre Tank
     * @param coorX suradnica x
     * @param coorY suradnica y
     * @param sizeOfBlock velkost bloku
     * @param hp health points pre Tank
     */
    public Tank(int coorX, int coorY, int sizeOfBlock, int hp, int damage) {

        super(coorX, coorY, "tank_w", sizeOfBlock);
        this.artilleryRange = 1;
        this.hp = hp;
        this.damage = damage;
        this.isDestroyed = false;
    }
    /**
     * Metoda changePicture prepisana - meni obrazok podla pohybu sipok
     */
    @Override
    public void changePicture(String imageName) {

        super.changePicture("tank_" + imageName);
    }

    /**
     * Getter pre damage
     */
    public int getDamage() {

        return this.damage;
    }

    public boolean lowerHp(int damage) {
        System.out.println("Tank is being attacked with damage: " + damage);
        if (this.hp - damage > 0) {
            this.hp -= damage;
            return false;
        } else {
            System.out.println("Tank is destroyed");
            this.isDestroyed = true;
            return true;
        }
    }
    /**
     * Getter pre artilleryRange - rozsah dostrelu tanku
     */
    public int getArtilleryRange() {

        return this.artilleryRange;
    }
    /**
     * Getter pre hp
     */
    public int getHp() {

        return this.hp;
    }

    /**
     * Metoda pre zvysenie utoku tanku
     * @param howMuch o kolko sa zvacsi utok
     */
    public void changeDamage(int howMuch) {
        System.out.println("Tank damage level increased");
        this.damage += howMuch;
    }

}
