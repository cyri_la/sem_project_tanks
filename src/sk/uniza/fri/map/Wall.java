package sk.uniza.fri.map;

/**
 * 28/03/2022 - 08:19
 * Trieda Wall - stena
 * potomok triedy Block
 */
public class Wall extends Block {
    /**
     * Konstruktor triedy Wall
     * @param coorX suradnica x
     * @param coorY suradnica y
     * @param sizeOfBlock velkost bloku
     */
    public Wall(int coorX, int coorY, int sizeOfBlock) {

        super(coorX, coorY, "wall", sizeOfBlock);
    }

}
